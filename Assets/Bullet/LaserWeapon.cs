﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class LaserWeapon : Weapon
{
    public override float GetCadencia()
    {
        return 1.0f;
    }
 
    public override void Shoot()
    {
        Debug.Log("Disparo laser");
    }
}